#!/bin/sh
#
# This script is an example hotplug script for use with the various
# input devices plugins.
#
# The script is called with the arguments:
# -t [added|present|removed] <device name>
#       added ... device was just plugged in
#       present.. device was present at gnome-settings-daemon startup
#       removed.. device was just removed
# -i <device ID>
#       device ID being the XInput device ID
# <device name> The name of the device
#
# The script should return 0 if the device is to be
# ignored from future configuration.
#
# Set the script to be used with:
# gsettings set org.gnome.settings-daemon.peripherals.input-devices hotplug-command /path/to/script/input-devices.sh
#
# see http://who-t.blogspot.com.au/2011/03/custom-input-device-configuration-in.html
# for better description

function set_added_configuration() {
logger -p user.info -t input-device-config  "set_added_configuration: \$1 = $1"
if [[ "$1" =~ ^Yubico ]] ; then
    logger -p user.info -t input-device-config \
        "Setting Yubikey layout"
    setxkbmap -device $2 -layout us
elif [[ "$1" =~ (USB\ Optical\ Mouse|Laser\ Mouse)$ ]] ; then
    logger -p user.info -t input-device-config \
        "Setting mouse eyes"
    gsettings set org.gnome.settings-daemon.peripherals.mouse \
        left-handed true
elif [[ "$1" =~ SynPS\/2\ Synaptics\ TouchPad ]] ; then
    logger -p user.info -t input-device-config \
        "Fixing touchpad on T440s"
    synclient RightButtonAreaLeft=0 RightButtonAreaTop=0
fi
}

function set_removed_configuration() {
case "$1" in
    *"USB Optical Mouse"|"* Laser Mouse")
        logger -p user.info -t input-device-config \
            "Upsetting mouse eyes"
        gsettings set org.gnome.settings-daemon.peripherals.mouse \
            left-handed false
        ;;
esac
}

args=$(getopt "t:i:" $*)

set -- $args

while [ $# -gt 0 ]
do
    case $1 in
        -t)
            shift;
            type="$1"
            ;;
        -i)
            shift;
            id="$1"
            ;;
        --)
            shift;
            device="$@"
            break;
            ;;
        *)
            echo "Unknown option $1";
            exit 1
            ;;
    esac
    shift
done

retval=0

case $type in
    added)
        logger -p user.info -t input-device-config \
            "Device '$device' (ID=$id) was added"
        set_added_configuration "$device" "$id"
        ;;
    present)
        logger -p user.info -t input-device-config \
            "Device '$device' (ID=$id) was already present at startup"
        set_added_configuration "$device" "$id"
        ;;
    removed)
        logger -p user.info -t input-device-config \
            "Device '$device' (ID=$id) was removed"
        set_removed_configuration "$device" "$id"
        ;;
    *)
        echo "Unknown operation"
        retval=1
        ;;
esac

# All further processing will be disabled if $retval == 1
exit $retval
